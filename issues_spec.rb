require 'rspec'
require 'fakeweb'
require './issues.rb'
require './issues_spec_support'

describe Issues do
  before(:each) do
    FakeWeb.register_uri(
    :any, 'https://api.github.com/repos/rails/rails/pulls/17892',
    :body => request_pull_request_1)
    FakeWeb.register_uri(
    :any, 'https://api.github.com/repos/rails/rails/pulls/17898',
    :body => request_pull_request_2)
    FakeWeb.register_uri(
      :any, 'https://api.github.com/repos/rails/rails/issues?state=closed',
      :body => request_body)
    @issues = Issues.new
  end

  it '#initialize' do
    issues = Issues.new

    expect(issues.data).to be_eql(JSON.parse HTTParty.get(Issues::URL))
  end

  it '#top_pull_requesters' do
    requester_1 = PullRequest.new(url: "https://api.github.com/repos/rails/rails/pulls/17898").requester_login
    requester_2 = PullRequest.new(url: "https://api.github.com/repos/rails/rails/pulls/17892").requester_login

    expect(@issues.top_pull_requesters).to be_eql([requester_1, requester_2])
  end

  it '#average_open_hours' do
    average = @issues.average_open_hours

    expect(average).to be_eql(2)
  end

  it '#open_hours' do
    created_at = @issues.convert_datetime(datetime: '2014-12-03T18:13:24Z')
    closed_at = @issues.convert_datetime(datetime: '2014-12-03T20:13:24Z')
    open_hours = @issues.open_hours(created_at: created_at, closed_at: closed_at)

    expect(open_hours).to be_eql(2)
  end

  it '#convert_datetime' do
    datetime_str = '2014-12-03T18:13:24Z'
    datetime = @issues.convert_datetime(datetime: datetime_str)
    expected = DateTime.strptime(datetime_str, '%Y-%m-%dT%H:%M:%SZ')

    expect(datetime).to be_eql(expected)
  end
end

describe PullRequest do
  before(:each) do
    FakeWeb.register_uri(
      :any, 'https://api.github.com/repos/rails/rails/pulls/17892',
      :body => request_pull_request_1
    )
    @pull_request = PullRequest.new(url: "https://api.github.com/repos/rails/rails/pulls/17892")
  end

  it '#initialize' do
    expected = JSON.parse(HTTParty.get("https://api.github.com/repos/rails/rails/pulls/17892"))

    expect(@pull_request.data).to be_eql(expected)
  end

  it '#requester_login' do
    expected = JSON.parse(HTTParty.get("https://api.github.com/repos/rails/rails/pulls/17892"))
    expected = expected['user']['login']

    expect(@pull_request.requester_login).to be_eql(expected)
  end
end
