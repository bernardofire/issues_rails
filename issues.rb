require 'httparty'

class Issues
  attr_accessor :data

  URL = 'https://api.github.com/repos/rails/rails/issues?state=closed'

  def initialize
    @data = JSON.parse HTTParty.get(URL).body
  end

  def top_pull_requesters
    requesters = {}
    @data.each do |issue|
      pull_request = issue['pull_request']
      if pull_request
        login = PullRequest.new(url: pull_request['url']).requester_login
        requesters[login] ||= 0
        requesters[login] += 1
      end
    end
    logins = []
    requesters.sort_by(&:last)[0..2].map { |e| logins << e[0] }
    logins
  end

  def average_open_hours
    total = 0
    @data.each do |issue|
      total += open_hours(created_at: convert_datetime(datetime: issue['created_at']),
                          closed_at: convert_datetime(datetime: issue['closed_at']))
    end
    total / @data.size
  end

  def open_hours(created_at: required, closed_at: required)
    ((closed_at - created_at) * 24 * 60 * 60).to_i / 3600
  end

  def convert_datetime(datetime: required)
    DateTime.strptime(datetime, '%Y-%m-%dT%H:%M:%SZ')
  end
end

class PullRequest
  attr_accessor :data

  def initialize(url: required)
    @data = JSON.parse HTTParty.get(url).body
  end

  def requester_login
    @data['user']['login']
  end
end
